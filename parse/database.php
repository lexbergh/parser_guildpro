<?
define('ROOT', dirname(__FILE__) . '/../');

class DataBase
{
    private static $db = null;
    private static $pdo;

    function __construct()
    {
        $params = parse_ini_file('db.ini');

        $host = $params['host'];
        $db = $params['dbname'];
        $user = $params['user'];
        $pass = $params['password'];
        $charset = $params['charset'];

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $this->pdo = new PDO($dsn, $user, $pass, $opt);
    }

    public static function getDB()
    {
        if (self::$db == null) self::$db = new DataBase();
        return self::$db;
    }

    function resultQuery($orderBy, $limit, $offset)
    {
        $sql = "SELECT `tag_id`, `tags`.`name`, SUM(`frequency`) AS frequencySum, COUNT(`page_id`) 
	  					AS pageCount, MAX(`frequency`) AS frequencyMax FROM `page_tags` 
	  					JOIN tags ON `page_tags`.`tag_id` = `tags`.`id`
	  					GROUP BY `tags`.`name` ORDER BY $orderBy DESC LIMIT ?,?";
        $statement = $this->pdo->prepare($sql);
        $statement->execute(array($offset, $limit));
        return $result = $statement->fetchAll();
    }

    function getPagesCount($limit)
    {
        $statement = $this->pdo->prepare("SELECT ceil(COUNT(*)/?) AS pageCount FROM `tags`");
        $statement->execute(array($limit));
        return $result = $statement->fetch();
    }

    function getPages()
    {
        return $statement = $this->pdo->query("SELECT `id`,`article` FROM `pages`")->fetchAll();
    }

    function getPageTags($pageId)
    {
        $statement = $this->pdo->prepare("SELECT `page_tags`.`id`,`tags`.`name` FROM page_tags JOIN tags ON `page_tags`.`tag_id` = `tags`.`id` WHERE `page_tags`.`page_id`=?");
        $statement->execute(array($pageId));
        $result = $statement->fetchAll();
        return $result;
    }

    function updatePageTagFrequency($tagId, $frequency)
    {
        $statement = $this->pdo->prepare("UPDATE page_tags SET frequency=? WHERE id=?");
        $statement->execute(array($frequency, $tagId));
    }

    function pageCounter()
    {
        return $statement = $this->pdo->query('SELECT * FROM pages')->rowCount();
    }

    function insertPageTag($tagId, $pageId)
    {
        $statement = $this->pdo->prepare("INSERT INTO page_tags(tag_id, page_id) VALUES (?,?)");
        $statement->execute(array($tagId, $pageId));
    }

    function insertPage($url, $h1, $article)
    {
        $statement = $this->pdo->prepare("INSERT INTO pages(url, h1, article) VALUES (?,?,?)");
        $statement->execute(array($url, $h1, $article));
        $id = $this->pdo->lastInsertId();
        return $id;
    }

    function insertTag($tagName)
    {
        $statement = $this->pdo->prepare("INSERT INTO tags(name) VALUES (?) ON DUPLICATE KEY UPDATE name=?");
        $statement->execute(array($tagName, $tagName));
        $statement = $this->pdo->prepare("SELECT id FROM tags WHERE name=?");
        $statement->execute(array($tagName));
        $id = $statement->fetch();
        return $id['id'];
    }

    function truncate()
    {
        $statement = $this->pdo->prepare("DELETE FROM page_tags");
        $statement->execute();
        $statement = $this->pdo->prepare("DELETE FROM tags");
        $statement->execute();
        $statement = $this->pdo->prepare("DELETE FROM pages");
        $statement->execute();
    }
}

?>