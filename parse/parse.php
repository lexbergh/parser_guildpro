<?
require '../vendor/autoload.php';
require 'database.php';

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;

libxml_use_internal_errors(true);

$time_start = microtime(true);


function fetchPages($startPage, $endPage)
{
    for ($id = $startPage; $id <= $endPage; $id++) {
        $url = 'https://autostat.ru/news/' . $id;
        yield new Request('GET', $url);
    }
}

function parsePage($body)
{
    $doc = new DOMDocument();
    $doc->loadHTML(mb_convert_encoding($body, 'HTML-ENTITIES', 'UTF-8'));
    $xpath = new DOMXPath($doc);

    $h1 = $xpath->query('//h1')[0]->nodeValue;
    $article = trim($xpath->query('//div[@itemprop="articleBody"]')[0]->nodeValue);
    $tags_a = $xpath->query('//span[@itemprop="keywords"]/a');
    $tags = array_map(
        function ($t) {
            if ($t->nodeValue) {
                return trim($t->nodeValue);
            }
        },
        iterator_to_array($tags_a)
    );

    $result = array(
        'h1' => $h1,
        'article' => $article,
        'tags' => $tags
    );
    return $result;
}

$db = DataBase::getDB();
$db->truncate();
$client = new Client(['http_errors' => false]);
$pool = new Pool($client, fetchPages(26000, 27000), [
    'concurrency' => 30,
    'fulfilled' => function ($response, $index) {
        $db = DataBase::getDB();
        if ($response->getStatusCode() == 200) {
            $parsedPage = parsePage($response->getBody());
            if ($parsedPage['h1'] and $parsedPage['article'] and $parsedPage['tags']) {
                $pageId = $db->insertPage($index, $parsedPage['h1'], $parsedPage['article']);
                foreach ($parsedPage['tags'] as $tag) {
                    if ($tag != '') {
                        $tagId = $db->insertTag($tag);
                        $db->insertPageTag($tagId, $pageId);
                    }
                }
            }
        }
        echo 'https://autostat.ru/news/' . (26000 + $index) . ' -> ' . $response->getStatusCode() . '<br/>';
        flush();
        ob_flush();
    },
]);

$promise = $pool->promise();
$promise->wait();

$time_end = microtime(true);
echo '--------------------------------------------- <br/>';
echo 'Страниц спарсилось: ' . $db->pageCounter();
$time = $time_end - $time_start;
echo '<br/>Время парсинга: ' . $time;
?>