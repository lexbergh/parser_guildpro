<?
require 'database.php';
$db = DataBase::getDB();

$pages = $db->getPages();

foreach ($pages as $page) {
    $pageTags = $db->getPageTags($page['id']);
    foreach ($pageTags as $pageTag) {
        $frequency = substr_count(mb_strtolower($page['article']), mb_strtolower($pageTag['name']));
        $db->updatePageTagFrequency($pageTag['id'], $frequency);
    }
}
echo 'Готово!';
?>