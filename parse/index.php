<?
require 'database.php';
$db = DataBase::getDB();
$page = $_GET['p'] < 0 ? 0 : $_GET['p'];
$orderBy = $_GET['orderBy'] == '' ? frequencySum : $_GET['orderBy'];
$limit = 10;
$offset = $page * $limit;
$table = $db->resultQuery($orderBy, $limit, $offset);
$pageCount = $db->getPagesCount($limit)['pageCount'];
?>
<!DOCTYPE html>
<html>
<head>
    <title>Parser</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css"/>
</head>
<body>
<div class="container">
    <div class="btn">
        <a class="btn btn-default" target="_blank" href="parse.php">Parse</a>
        <a class="btn btn-default" target="_blank" href="calc.php">Calc weight</a>
    </div>
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th class="text-left">id</th>
            <th class="text-left">Название метки</th>
            <th class="text-left"><a href="?orderBy=frequencySum">Общий вес</a></th>
            <th class="text-left"><a href="?orderBy=pageCount">Кол-во публикаций</a></th>
            <th class="text-left"><a href="?orderBy=frequencyMax">Макс. вес в публикации</a></th>
        </tr>
        </thead>
        <tbody class="table">
        <?
        foreach ($table as $key => $string) {
            echo '<tr><td class="text-left">' . $string['tag_id'] .
                '<td class="text-left">' . $string['name'] .
                '</td><td class="text-left">' . $string['frequencySum'] .
                '</td><td class="text-left">' . $string['pageCount'] .
                '</td><td class="text-left">' . $string['frequencyMax'] .
                '</td></tr>';
        }
        ?>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <?
            for ($i = 0; $i < $pageCount; $i++) {
                $class = $i == $_GET['p'] ? 'active' : '';
                echo '<li class="' . $class . '"><a style="margin-right: 5px" href="?p=' . $i . '&orderBy=' . $orderBy . '">' . ($i + 1) . '</a></li>';
            }
            ?>
        </ul>
    </nav>
</div>
</body>
</html>